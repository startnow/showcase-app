// React
import React from 'react'

// Components
import { ScrollView, Button, Text, StyleSheet, StatusBar, View } from 'react-native'

// Apollo
import { useQuery, useMutation, useSubscription } from 'react-apollo'
import * as graph from '../../graphql/requests'

// Utils
import * as shortid from 'shortid'


const Comments = () => {

    const subscription = useSubscription(
        graph.COMMENT_ADDED,
        { variables: { author: 'Armand' } }
    );

    const { loading, error, data, refetch } = useQuery(graph.COMMENTS);
    const [addComment] = useMutation(graph.ADD_COMMENT);
    

    return (
        <ScrollView style={styles.container}>

                <StatusBar barStyle='light-content' />

                <View style={{
                    backgroundColor: 'white',
                    margin: 20,
                }}>
                    <Text>
                        Subscription:
                        {!subscription.loading && !subscription.error && (
                            JSON.stringify(subscription.data, null, 4)
                        )}
                    </Text>
                </View>

                <Button
                    title='Add comment'
                    onPress={async () => {

                        const input = {
                            content: `content_${shortid.generate()}`,
                            author: 'Armand',
                        };

                        await addComment({ variables: { input } });
                    }}
                />

                <Button
                    title='Refresh data'
                    onPress={async () => await refetch()}
                />

                {loading && (
                    <Text style={{ color: 'white' }}>
                        LOADING
                    </Text>
                )}

                {error && (
                    <Text style={{ color: 'white' }}>
                        ERROR
                    </Text>
                )}

                {!loading && !error && (
                    <Text style={{ color: 'red', fontSize: 20 }}>
                        {JSON.stringify(data.comments)}
                    </Text>
                )}

            </ScrollView>
    )
}

export default Comments

// Styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#000',
        paddingTop: 30,
        flex: 1,
    },
})