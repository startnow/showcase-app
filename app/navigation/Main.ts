// Navigation
import { createStackNavigator } from 'react-navigation-stack'

// Screens
import Comments from '../screens/Comments'


const MainStack = createStackNavigator(
    {
        Comments: {
            screen: Comments,
        },
    },
    {
        headerMode: 'none',
        navigationOptions: ({ navigation }) => {
            const tabBarVisible = navigation.state.index === 0;
            return { tabBarVisible };
        },
    }
)

export default MainStack