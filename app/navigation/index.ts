// Navigation
import { createAppContainer } from 'react-navigation'

// Screens
import Main from './Main'


export default createAppContainer(Main)