import Constants from 'expo-constants'


// Retrieve channel
// releaseChannel is undefined on development
const { releaseChannel } = Constants.manifest;

const envChannel = process.env.REACT_NATIVE_CHANNEL;

const fullChannel = releaseChannel
    || envChannel
    || 'local';

const pattern = /(\w+)(-v([0-9](\.[0-9])+))?/i;

const matchs = fullChannel.match(pattern);
if (!matchs) throw 'Cannot select channel';

const version = matchs.length > 3 && matchs[3];
const channel = matchs[1];

const shared = {}

const environments = {

    local: {
        GRAPHQL_WS_URI: 'http://localhost:3000/graphql',
        GRAPHQL_URI: 'http://localhost:3000/graphql',
    },
    prod: {
        GRAPHQL_WS_URI: 'http://localhost:3000/graphql',
        GRAPHQL_URI: 'http://localhost:3000/graphql',
    },
}

const current = { ...shared, ...environments[channel] }

export default current

console.log(`channel: ${channel}`);