// React
import React from 'react'

// Components
import AppNavigator from './app/navigation'

// Apollo
import { ApolloProvider } from 'react-apollo'
import client from './app/graphql/client'


export default function App() {
    return (
        <ApolloProvider client={client}>
            <AppNavigator />
        </ApolloProvider>
    )
}